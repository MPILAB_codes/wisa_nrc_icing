def set_size(width, fraction=1):
    """ Set aesthetic figure dimensions to avoid scaling in latex.

    Parameters
    ----------
    width: float
            Width in pts
    fraction: float
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim
def set_twowide(width, fraction=1):
    """ Set aesthetic figure dimensions to avoid scaling in latex.

    Parameters
    ----------
    width: float
            Width in pts
    fraction: float
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt*1.2
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio/1.5

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim



import os
print(os.environ['PATH'])
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt


width = 468
myformat='.pdf'

nice_fonts = {
      # Use LaTex to write all text
        "text.usetex": True,
        "font.family": "serif",
      # Use 10pt font in plots, to match 10pt font in document
        "axes.labelsize": 11,
        "font.size": 11,
      # Make the legend/label fonts a little smaller
        "legend.fontsize": 10,
        "xtick.labelsize": 10,
        "ytick.labelsize": 10,
}

mpl.rcParams.update(nice_fonts)
lstyle = {  '0.5\%c':0.25,
            '0.75\%c':0.35,
             '1.0\%c':0.45,
            '1.5\%c':0.65,
            '2.5\%c':0.85,
            '5.0\%c':1}
            
cdict = {'0.5\%c': '#FED976',
            '0.75\%c':'#FEB24C',
            '1.0\%c':'#FD8D3C',
            '1.5\%c':'#FC4E2A', 
            '2.5\%c':'#E31A1C',
            '5.0\%c': '#B10026'}

rstyle = {
     'g6':       (0, (5, 0.5)),
     'g5':        (0, (5, 1)),
     'g4':        (0, (5, 5)),
     'g3':        (0, (5, 10)),
     'g2':          (0, (3, 1, 1, 1)),
     'g1':        (0, (3, 1, 1, 1, 1, 1))}


#data_t05=np.loadtxt("thick_025/SU2_HORN_t025.dat", unpack=True)  
data_GLAZE_075_noIce=np.loadtxt("glaze-0.0075-0.015-0.015/SU2_t025_d015.dat", unpack=True)  
data_GLAZE_075_r1=np.loadtxt("glaze-0.0075-0.015-0.015/glaze1.dat", unpack=True)   
data_GLAZE_075_r2=np.loadtxt("glaze-0.0075-0.015-0.015/glaze2.dat", unpack=True)   
data_GLAZE_075_r3=np.loadtxt("glaze-0.0075-0.015-0.015/glaze3.dat", unpack=True)   
data_GLAZE_075_r4=np.loadtxt("glaze-0.0075-0.015-0.015/glaze4.dat", unpack=True)   
data_GLAZE_075_r5=np.loadtxt("glaze-0.0075-0.015-0.015/glaze5.dat", unpack=True)   
data_GLAZE_075_r6=np.loadtxt("glaze-0.0075-0.015-0.015/glaze6.dat", unpack=True)  

data_RIME_015_noIce=np.loadtxt("rime-0.015-0.025/SU2_RIM_thick015.dat", unpack=True)  
data_RIME_015_r1=np.loadtxt("rime-0.015-0.025/rime1.dat", unpack=True)   
data_RIME_015_r2=np.loadtxt("rime-0.015-0.025/rime2.dat", unpack=True)   
data_RIME_015_r3=np.loadtxt("rime-0.015-0.025/rime3.dat", unpack=True)   
data_RIME_015_r4=np.loadtxt("rime-0.015-0.025/rime4.dat", unpack=True)   
data_RIME_015_r5=np.loadtxt("rime-0.015-0.025/rime5.dat", unpack=True)   
data_RIME_015_r6=np.loadtxt("rime-0.015-0.025/rime6.dat", unpack=True)   

data_HORN_015_noIce=np.loadtxt("horn-0.015-0.005/SU2_HORN_t015.dat", unpack=True)  
data_HORN_015_r1=np.loadtxt("horn-0.015-0.005/horn1.dat",delimiter=",", unpack=True)   
data_HORN_015_r2=np.loadtxt("horn-0.015-0.005/horn2.dat", delimiter=",",unpack=True)   
data_HORN_015_r3=np.loadtxt("horn-0.015-0.005/horn3.dat",delimiter=",", unpack=True)   
data_HORN_015_r4=np.loadtxt("horn-0.015-0.005/horn4.dat",delimiter=",", unpack=True)   
data_HORN_015_r5=np.loadtxt("horn-0.015-0.005/horn5.dat", delimiter=",",unpack=True)   
data_HORN_015_r6=np.loadtxt("horn-0.015-0.005/horn6.dat", delimiter=",",unpack=True)   


dataSU2_valid=np.loadtxt("../SU2_validation/validationData/SU2_superFine.dat", unpack=True)

import pdb

fig, [ax1,ax2] = plt.subplots(1,2,figsize=set_twowide(width))
ax1.scatter(dataSU2_valid[0,:],dataSU2_valid[11,:],marker="o",color='b', label="no ice",facecolor='none')
ax1.scatter(-dataSU2_valid[0,:],-dataSU2_valid[11,:],marker="o",color='b', facecolor='none')


ax1.plot(-data_GLAZE_075_noIce[0,:],-data_GLAZE_075_noIce[11,:],color='k', label="smooth")
ax1.plot(data_GLAZE_075_r6[0,:],-data_GLAZE_075_r6[11,:],color=cdict['0.75\%c'],linestyle=rstyle['g6'], label="0.025\%c")
ax1.plot(data_GLAZE_075_r5[0,:],-data_GLAZE_075_r5[11,:],color=cdict['0.75\%c'],linestyle=rstyle['g5'], label="0.1\%c")
ax1.plot(data_GLAZE_075_r4[0,:],-data_GLAZE_075_r4[11,:],color=cdict['0.75\%c'],linestyle=rstyle['g4'], label="0.5\%c")
ax1.plot(data_GLAZE_075_r3[0,:],-data_GLAZE_075_r3[11,:],color=cdict['0.75\%c'],linestyle=rstyle['g3'], label="1.0\%c")
ax1.plot(data_GLAZE_075_r2[0,:],-data_GLAZE_075_r2[11,:],color=cdict['0.75\%c'],linestyle=rstyle['g2'], label="5.0\%c")
#ax1.plot(data_GLAZE_075_r1[0,:],-data_GLAZE_075_r1[11,:],color=cdict['0.75\%c'],linestyle=rstyle['g1'])





ax1.set_ylim(0,1.35)
ax1.set_xlim(0,13.5)
ax1.legend()
ax1.set_xlabel("Angle of attack (degree)")
ax1.set_ylabel(r"$C_l$")



ax2.scatter(-dataSU2_valid[11,:],-dataSU2_valid[10,:],marker="o",color='b', facecolor='none')
ax2.scatter(-dataSU2_valid[11,:],dataSU2_valid[10,:],marker="o",color='b',facecolor='none')

ax2.plot(-data_GLAZE_075_noIce[11,:],data_GLAZE_075_noIce[10,:],color='k')

#ax2.plot(-data_GLAZE_075_r1[11,:],data_GLAZE_075_r1[10,:],color=cdict['0.75\%c'],linestyle=rstyle['g1'])
ax2.plot(-data_GLAZE_075_r2[11,:],data_GLAZE_075_r2[10,:],color=cdict['0.75\%c'],linestyle=rstyle['g2'])
ax2.plot(-data_GLAZE_075_r3[11,:],data_GLAZE_075_r3[10,:],color=cdict['0.75\%c'],linestyle=rstyle['g3'])
ax2.plot(-data_GLAZE_075_r4[11,:],data_GLAZE_075_r4[10,:],color=cdict['0.75\%c'],linestyle=rstyle['g4'])
ax2.plot(-data_GLAZE_075_r5[11,:],data_GLAZE_075_r5[10,:],color=cdict['0.75\%c'],linestyle=rstyle['g5'])
ax2.plot(-data_GLAZE_075_r6[11,:],data_GLAZE_075_r6[10,:],color=cdict['0.75\%c'],linestyle=rstyle['g6'])
ax2.set_ylim(0,0.03)
ax2.set_xlim(0,1.5)
ax1.legend()
ax2.set_ylabel(r"$C_d$")
ax2.set_xlabel(r"$C_l$")
plt.tight_layout()
plt.savefig("Glaze_t075_roughness"+myformat)




fig, [ax1,ax2] = plt.subplots(1,2,figsize=set_twowide(width))
ax1.scatter(dataSU2_valid[0,:],dataSU2_valid[11,:],marker="o",color='b', label="no ice",facecolor='none')
ax1.scatter(-dataSU2_valid[0,:],-dataSU2_valid[11,:],marker="o",color='b',facecolor='none')


ax1.plot(-data_RIME_015_noIce[0,:],-data_RIME_015_noIce[10,:],color='k', label="smooth")
ax1.plot(data_RIME_015_r6[0,:],-data_RIME_015_r6[11,:],color=cdict['0.75\%c'],linestyle=rstyle['g6'], label="0.025\%c")
ax1.plot(data_RIME_015_r5[0,:],-data_RIME_015_r5[11,:],color=cdict['0.75\%c'],linestyle=rstyle['g5'], label="0.1\%c")
ax1.plot(data_RIME_015_r4[0,:],-data_RIME_015_r4[11,:],color=cdict['0.75\%c'],linestyle=rstyle['g4'], label="0.5\%c")
ax1.plot(data_RIME_015_r3[0,:],-data_RIME_015_r3[11,:],color=cdict['0.75\%c'],linestyle=rstyle['g3'], label="1.0\%c")
#ax1.plot(data_RIME_015_r2[0,:],-data_RIME_015_r2[11,:],color=cdict['0.75\%c'],linestyle=rstyle['g2'], label="50.0 mm")
#ax1.plot(data_RIME_015_r1[0,:],-data_RIME_015_r1[11,:],color=cdict['0.75\%c'],linestyle=rstyle['g1'])

ax1.set_ylim(0,1.5)
ax1.set_xlim(0,14.5)
ax1.legend()
ax1.set_xlabel("Angle of attack (degree)")
ax1.set_ylabel(r"$C_l$")



ax2.scatter(-dataSU2_valid[11,:],-dataSU2_valid[10,:],marker="o",color='b', label="no ice",facecolor='none')
ax2.scatter(-dataSU2_valid[11,:],dataSU2_valid[10,:],marker="o",color='b', label="no ice",facecolor='none' )

ax2.plot(-data_RIME_015_noIce[10,:],data_RIME_015_noIce[9,:],color='k')

#ax2.plot(-data_RIME_015_r1[11,:],data_RIME_015_r1[10,:],color=cdict['0.75\%c'],linestyle=rstyle['g1'])
#ax2.plot(-data_RIME_015_r2[11,:],data_RIME_015_r2[10,:],color=cdict['0.75\%c'],linestyle=rstyle['g2'])
ax2.plot(-data_RIME_015_r3[11,:],data_RIME_015_r3[10,:],color=cdict['0.75\%c'],linestyle=rstyle['g3'])
ax2.plot(-data_RIME_015_r4[11,:],data_RIME_015_r4[10,:],color=cdict['0.75\%c'],linestyle=rstyle['g4'])
ax2.plot(-data_RIME_015_r5[11,:],data_RIME_015_r5[10,:],color=cdict['0.75\%c'],linestyle=rstyle['g5'])
ax2.plot(-data_RIME_015_r6[11,:],data_RIME_015_r6[10,:],color=cdict['0.75\%c'],linestyle=rstyle['g6'])
ax2.set_ylim(0,0.03)
ax2.set_xlim(0,1.5)
ax1.legend()
ax2.set_ylabel(r"$C_d$")
ax2.set_xlabel(r"$C_l$")
plt.tight_layout()
plt.savefig("RIME_t015_roughness"+myformat)



#HORN ICING


fig, [ax1,ax2] = plt.subplots(1,2,figsize=set_twowide(width))
ax1.scatter(dataSU2_valid[0,:],dataSU2_valid[11,:],marker="o",color='b', label="no ice",facecolor='none')
ax1.scatter(-dataSU2_valid[0,:],-dataSU2_valid[11,:],marker="o",color='b',facecolor='none')


ax1.plot(-data_HORN_015_noIce[0,:],-data_HORN_015_noIce[10,:],color='k', label="smooth")
ax1.plot(data_HORN_015_r6[0,:],-data_HORN_015_r6[11,:],color=cdict['0.75\%c'],linestyle=rstyle['g6'], label="0.025\%c")
ax1.plot(data_HORN_015_r5[0,:],-data_HORN_015_r5[11,:],color=cdict['0.75\%c'],linestyle=rstyle['g5'], label="0.1\%c")
ax1.plot(data_HORN_015_r4[0,:],-data_HORN_015_r4[11,:],color=cdict['0.75\%c'],linestyle=rstyle['g4'], label="0.5\%c")
ax1.plot(data_HORN_015_r3[0,:],-data_HORN_015_r3[11,:],color=cdict['0.75\%c'],linestyle=rstyle['g3'], label="1.0\%c")
#ax1.plot(data_HORN_015_r2[0,:],-data_HORN_015_r2[11,:],color=cdict['0.75\%c'],linestyle=rstyle['g2'], label="50.0 mm")
#ax1.plot(data_HORN_015_r1[0,:],-data_HORN_015_r1[11,:],color=cdict['0.75\%c'],linestyle=rstyle['g1'])

ax1.set_ylim(0,1.5)
ax1.set_xlim(0,14.5)
ax1.legend()
ax1.set_xlabel("Angle of attack (degree)")
ax1.set_ylabel(r"$C_l$")



ax2.scatter(-dataSU2_valid[11,:],-dataSU2_valid[10,:],marker="o",color='b', label="no ice",facecolor='none')
ax2.scatter(-dataSU2_valid[11,:],dataSU2_valid[10,:],marker="o",color='b', label="no ice",facecolor='none' )

ax2.plot(-data_HORN_015_noIce[10,:],data_HORN_015_noIce[9,:],color='k')

#ax2.plot(-data_HORN_015_r1[11,:],data_HORN_015_r1[10,:],color=cdict['0.75\%c'],linestyle=rstyle['g1'])
#ax2.plot(-data_HORN_015_r2[11,:],data_HORN_015_r2[10,:],color=cdict['0.75\%c'],linestyle=rstyle['g2'])
ax2.plot(-data_HORN_015_r3[11,:],data_HORN_015_r3[10,:],color=cdict['0.75\%c'],linestyle=rstyle['g3'])
ax2.plot(-data_HORN_015_r4[11,:],data_HORN_015_r4[10,:],color=cdict['0.75\%c'],linestyle=rstyle['g4'])
ax2.plot(-data_HORN_015_r5[11,:],data_HORN_015_r5[10,:],color=cdict['0.75\%c'],linestyle=rstyle['g5'])
ax2.plot(-data_HORN_015_r6[11,:],data_HORN_015_r6[10,:],color=cdict['0.75\%c'],linestyle=rstyle['g6'])
ax2.set_ylim(0,0.03)
ax2.set_xlim(0,1.5)
ax1.legend()
ax2.set_ylabel(r"$C_d$")
ax2.set_xlabel(r"$C_l$")
plt.tight_layout()
plt.savefig("HORN_t015_roughness"+myformat)


