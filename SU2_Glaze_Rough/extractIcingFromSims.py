mypath='/scratch/j/jphickey/p2nath/SU2/roughice/glaze-0.0075-0.015-0.015/glaze5/DIRECT_M_0.15_AOA_'

angles=[0.0, 2.0,3.0,4.0,5.0,6.0,7.0,7.5,8.0,8.25,8.5,8.75,9.0,9.25,9.5,9.75,10.0,10.25,10.5,10.75,11.0,11.25,11.5,11.75,12.0,12.5,13.0,14]

for filename in angles:

    with open (mypath+str(filename)+"/history_direct.csv", 'r') as handle:
        for line in handle:
            pass
        # print the last one only
        print(str(filename) +" , "+line.rstrip('\r\n'))

    if filename==0.0:mypath=mypath+"-"
