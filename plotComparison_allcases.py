def set_size(width, fraction=1):
    """ Set aesthetic figure dimensions to avoid scaling in latex.

    Parameters
    ----------
    width: float
            Width in pts
    fraction: float
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim
def set_twowide(width, fraction=1):
    """ Set aesthetic figure dimensions to avoid scaling in latex.

    Parameters
    ----------
    width: float
            Width in pts
    fraction: float
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio/1.08

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim



import os
print(os.environ['PATH'])
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

from scipy import interpolate


width = 468
myformat='.pdf'

nice_fonts = {
      # Use LaTex to write all text
        "text.usetex": True,
        "font.family": "serif",
      # Use 10pt font in plots, to match 10pt font in document
        "axes.labelsize": 11,
        "font.size": 11,
      # Make the legend/label fonts a little smaller
        "legend.fontsize": 10,
        "xtick.labelsize": 10,
        "ytick.labelsize": 10,
}

mpl.rcParams.update(nice_fonts)



            
            
#lstyle = {  '0.005':'solid',
#            '0.0075':'dotted',
#            '0.01':'dashed',
#            '0.015':'dashdot',
#            '0.025':(0, (3, 1, 1, 1)),
#            '0.05':(0, (5, 1))}
lstyle = {  '0.005':0.25,
            '0.0075':0.35,
             '0.01':0.45,
            '0.015':0.65,
            '0.025':0.85,
            '0.05':1}

cdict={'glaze':'r','rim':'b','horn':'g'}#'dashed'}#'dashdot '

import pdb

            
data_t005_H  =np.loadtxt("SU2_HORN_thickness/thick_005/SU2_HORN_t005.dat", unpack=True)  
data_t01_H   =np.loadtxt("SU2_HORN_thickness/thick_01/SU2_HORN_t01.dat", unpack=True)  
data_t025_H  =np.loadtxt("SU2_HORN_thickness/thick_025/SU2_HORN_t025.dat", unpack=True)   
data_t015_H  =np.loadtxt("SU2_HORN_thickness/thick_015/SU2_HORN_t015.dat", unpack=True)    # RUN ON CERTAINTY!!! 
data_t0075_H =np.loadtxt("SU2_HORN_thickness/thick_0075/SU2_HORN_t0075.dat", unpack=True) # RUN ON CERTAINTY!!! 


data_t05_R   =np.loadtxt("SU2_RIM_thickness/thick_05/SU2_RIM_thick05.dat", unpack=True)  
data_t025_R  =np.loadtxt("SU2_RIM_thickness/thick_025/SU2_RIM_thick025.dat", unpack=True)   # RUN ON CERTAINTY!!! 
data_t015_R  =np.loadtxt("SU2_RIM_thickness/thick_015/SU2_RIM_thick015.dat", unpack=True)    # RUN ON CERTAINTY!!! 
data_t0075_R =np.loadtxt("SU2_RIM_thickness/thick_0075/SU2_RIM_thick0075.dat", unpack=True)


data_t01_G   =np.loadtxt("SU2_thickness_d025/thick_01/SU2_t01_d025.dat", unpack=True)
data_t005_G  =np.loadtxt("SU2_thickness_d025/thick_005/SU2_t005_d025.dat", unpack=True)
data_t0075_G =np.loadtxt("SU2_thickness_d025/thick_0075/SU2_t0075_d025.dat", unpack=True)

dataSU2_valid=np.loadtxt("SU2_validation/validationData/SU2_superFine.dat", unpack=True)

func_noIce_Cl = interpolate.interp1d(-dataSU2_valid[0,:],-dataSU2_valid[11,:],kind='quadratic')
func_noIce_Cd = interpolate.interp1d(-dataSU2_valid[0,:],-dataSU2_valid[11,:]/dataSU2_valid[10,:],kind='quadratic') #LAZY 
func_noIce_ClCd = interpolate.interp1d(-dataSU2_valid[0,:],-dataSU2_valid[11,:]/dataSU2_valid[10,:],kind='quadratic')



angles=np.linspace(1,-dataSU2_valid[0,-1],100)

def Ang(f,angles):
  maxAngletemp=func_noIce_Cl(angles)
  return angles[np.argmax(maxAngletemp)]


func_t005_H = interpolate.interp1d(-data_t005_H[0,:],-data_t005_H[11,:],kind='quadratic')
func_t01_H = interpolate.interp1d(-data_t01_H[0,:],-data_t01_H[11,:],kind='quadratic')
func_t025_H = interpolate.interp1d(-data_t025_H[0,:],-data_t025_H[11,:],kind='quadratic')
func_t015_H = interpolate.interp1d(-data_t015_H[0,:],-data_t015_H[10,:],kind='quadratic')
func_t0075_H = interpolate.interp1d(-data_t0075_H[0,:],-data_t0075_H[10,:],kind='quadratic')

func_t05_R = interpolate.interp1d(-data_t05_R[0,:],-data_t05_R[11,:],kind='quadratic')
func_t025_R = interpolate.interp1d(-data_t025_R[0,:],-data_t025_R[10,:],kind='quadratic')
func_t015_R = interpolate.interp1d(-data_t015_R[0,:],-data_t015_R[10,:],kind='quadratic')
func_t0075_R = interpolate.interp1d(-data_t0075_R[0,:],-data_t0075_R[10,:],kind='quadratic')

func_t01_G = interpolate.interp1d(-data_t01_G[0,:],-data_t01_G[11,:],kind='quadratic')
func_t005_G = interpolate.interp1d(-data_t005_G[0,:],-data_t005_G[10,:],kind='quadratic')
func_t0075_G = interpolate.interp1d(-data_t0075_G[0,:],-data_t0075_G[10,:],kind='quadratic')

maxAngle=Ang(func_noIce_Cl,np.linspace(0.1,-dataSU2_valid[0,-1],100))

Horn_angle=np.zeros(6)
Horn_thick=[0,0.005,0.0075,0.01,0.015,0.025]
Horn_angle[0]=maxAngle
Horn_angle[1]=Ang(func_t005_H,np.linspace(0.1,-data_t005_H[0,-1],100))
Horn_angle[2]=Ang(func_t0075_H,np.linspace(0.1,-data_t0075_H[0,-1],100))
Horn_angle[3]=Ang(func_t01_H,np.linspace(0.1,-data_t01_H[0,-1],100))
Horn_angle[4]=Ang(func_t015_H,np.linspace(0.1,-data_t015_H[0,-1],100))
Horn_angle[5]=Ang(func_t025_H,np.linspace(0.1,-data_t025_H[0,-1],100))



Rime_angle=np.zeros(5)
Rime_thick=[0,0.0075,0.015,0.025,0.05]
Rime_angle[0]=maxAngle
Rime_angle[1]=Ang(func_t0075_R,np.linspace(0.1,-data_t0075_R[0,-1],100))
Rime_angle[2]=Ang(func_t015_R,np.linspace(0.1,-data_t015_R[0,-1],100))
Rime_angle[3]=Ang(func_t025_R,np.linspace(0.1,-data_t025_R[0,-1],100))
Rime_angle[4]=Ang(func_t05_R,np.linspace(0.1,-data_t05_R[0,-1],100))


Glaze_angle=np.zeros(4)
Glaze_thick=[0,0.005,0.0075,0.01]
Glaze_angle[0]=maxAngle
Glaze_angle[1]=Ang(func_t005_G,np.linspace(0.1,-data_t005_G[0,-1],100))
Glaze_angle[2]=Ang(func_t0075_G,np.linspace(0.1,-data_t0075_G[0,-1],100))
Glaze_angle[3]=Ang(func_t01_G,np.linspace(0.1,-data_t01_G[0,-1],100))

pdb.set_trace()

fig, [ax1,ax2] = plt.subplots(1,2,figsize=set_twowide(width))
myval=-0.11
myvalCd=-10
Horn_t005_angle =-data_t005_H[0,:]
Horn_t005_Cl    =-data_t005_H[11,:]
Horn_t005_Cd    =-data_t005_H[11,:]
fHorn_t005_Cl    = interpolate.interp1d(Horn_t005_Cl-func_noIce_Cl(Horn_t005_angle),Horn_t005_angle,kind='quadratic')
fHorn_t005_Cd    = interpolate.interp1d((Horn_t005_Cl/Horn_t005_Cd-func_noIce_Cd(Horn_t005_angle)),Horn_t005_angle,kind='quadratic')
Horn_t01_angle  =-data_t01_H[0,:]
Horn_t01_Cl     =-data_t01_H[11,:]
Horn_t01_Cd     =-data_t01_H[11,:]
fHorn_t01_Cl    = interpolate.interp1d(Horn_t01_Cl-func_noIce_Cl(Horn_t01_angle),Horn_t01_angle,kind='quadratic')
fHorn_t01_Cd    = interpolate.interp1d((Horn_t01_Cl/Horn_t01_Cd-func_noIce_Cd(Horn_t01_angle)),Horn_t01_angle,kind='quadratic')
Horn_t025_angle =-data_t025_H[0,:]
Horn_t025_Cl    =-data_t025_H[11,:]
Horn_t025_Cd    =-data_t025_H[10,:]
fHorn_t025_Cl    = interpolate.interp1d(Horn_t025_Cl-func_noIce_Cl(Horn_t025_angle),Horn_t025_angle,kind='quadratic')
fHorn_t025_Cd    = interpolate.interp1d((Horn_t025_Cl/Horn_t025_Cd-func_noIce_Cd(Horn_t025_angle)),Horn_t025_angle,kind='quadratic')
Horn_t015_angle =-data_t015_H[0,:]
Horn_t015_Cl    =-data_t015_H[10,:]
Horn_t015_Cd    =-data_t015_H[9,:]
fHorn_t015_Cl    = interpolate.interp1d(Horn_t015_Cl-func_noIce_Cl(Horn_t015_angle),Horn_t015_angle,kind='quadratic')
fHorn_t015_Cd    = interpolate.interp1d((Horn_t015_Cl/Horn_t015_Cd-func_noIce_Cd(Horn_t015_angle)),Horn_t015_angle,kind='quadratic')
Horn_t0075_angle =-data_t0075_H[0,:]
Horn_t0075_Cl    =-data_t0075_H[10,:]
Horn_t0075_Cd    =-data_t0075_H[9,:]
fHorn_t0075_Cl    = interpolate.interp1d(Horn_t0075_Cl-func_noIce_Cl(Horn_t0075_angle),Horn_t0075_angle,kind='quadratic')
fHorn_t0075_Cd    = interpolate.interp1d((Horn_t0075_Cl/Horn_t0075_Cd-func_noIce_Cd(Horn_t0075_angle)),Horn_t0075_angle,kind='quadratic')
HornPenalty_Cl=np.array([[0.005,fHorn_t005_Cl(myval)],[0.0075,fHorn_t0075_Cl(myval)],[0.01,fHorn_t01_Cl(myval)],[0.015,fHorn_t015_Cl(myval)],[0.025,fHorn_t025_Cl(myval)]])
HornPenalty_Cd=np.array([[0.005,fHorn_t005_Cd(myvalCd)],[0.0075,fHorn_t0075_Cd(myvalCd)],[0.01,fHorn_t01_Cd(myvalCd)],[0.015,fHorn_t015_Cd(myvalCd)],[0.025,fHorn_t025_Cd(myvalCd)]])

Rim_t0075_angle =-data_t0075_R[0,:]
Rim_t0075_Cl    =-data_t0075_R[11,:]
Rim_t0075_Cd    =-data_t0075_R[10,:]
fRim_t0075_Cl   = interpolate.interp1d(Rim_t0075_Cl-func_noIce_Cl(Rim_t0075_angle),Rim_t0075_angle,kind='quadratic')
fRim_t0075_Cd   = interpolate.interp1d((Rim_t0075_Cl/Rim_t0075_Cd-func_noIce_Cd(Rim_t0075_angle)),Rim_t0075_angle,kind='quadratic')
Rim_t015_angle =-data_t015_R[0,:]
Rim_t015_Cl    =-data_t015_R[10,:]
Rim_t015_Cd    =-data_t015_R[9,:]
fRim_t015_Cl   = interpolate.interp1d(Rim_t015_Cl-func_noIce_Cl(Rim_t015_angle),Rim_t015_angle,kind='quadratic')
fRim_t015_Cd   = interpolate.interp1d((Rim_t015_Cl/Rim_t015_Cd-func_noIce_Cd(Rim_t015_angle)),Rim_t015_angle,kind='quadratic')
Rim_t025_angle =-data_t025_R[0,:]
Rim_t025_Cl    =-data_t025_R[10,:]
Rim_t025_Cd    =-data_t025_R[9,:]
fRim_t025_Cl   = interpolate.interp1d(Rim_t025_Cl-func_noIce_Cl(Rim_t025_angle),Rim_t025_angle,kind='quadratic')
fRim_t025_Cd   = interpolate.interp1d((Rim_t025_Cl/Rim_t025_Cd-func_noIce_Cd(Rim_t025_angle)),Rim_t025_angle,kind='quadratic')
Rim_t05_angle =-data_t05_R[0,:]
Rim_t05_Cl    =-data_t05_R[11,:]
Rim_t05_Cd    =-data_t05_R[10,:]
fRim_t05_Cl   = interpolate.interp1d(Rim_t05_Cl-func_noIce_Cl(Rim_t05_angle),Rim_t05_angle,kind='quadratic')
fRim_t05_Cd   = interpolate.interp1d((Rim_t05_Cl/Rim_t05_Cd-func_noIce_Cd(Rim_t05_angle)),Rim_t05_angle,kind='quadratic')
RimPenalty_Cl=np.array([[0.0075,fRim_t0075_Cl(myval)],[0.015,fRim_t015_Cl(myval)],[0.025,fRim_t025_Cl(myval)],[0.05,fRim_t05_Cl(myval)]])

RimPenalty_Cd=np.array([[0.015,fRim_t015_Cd(myvalCd)],[0.025,fRim_t025_Cd(myvalCd)],[0.05,fRim_t05_Cd(myvalCd)]])

Glaze_t0075_angle= -data_t0075_G[0,:]
Glaze_t0075_Cl= -data_t0075_G[11,:]
Glaze_t0075_Cd= -data_t0075_G[10,:]
fGlaze_t0075_Cl   = interpolate.interp1d(Glaze_t0075_Cl-func_noIce_Cl(Glaze_t0075_angle),Glaze_t0075_angle,kind='quadratic')
fGlaze_t0075_Cd   = interpolate.interp1d((Glaze_t0075_Cl/Glaze_t0075_Cd-func_noIce_Cd(Glaze_t0075_angle)),Glaze_t0075_angle,kind='quadratic')
Glaze_t005_angle =-data_t005_G[0,:]
Glaze_t005_Cl= -data_t005_G[11,:]
Glaze_t005_Cd= -data_t005_G[10,:]
fGlaze_t005_Cl   = interpolate.interp1d(Glaze_t005_Cl-func_noIce_Cl(Glaze_t005_angle),Glaze_t005_angle,kind='quadratic')
fGlaze_t005_Cd   = interpolate.interp1d((Glaze_t005_Cl/Glaze_t005_Cd-func_noIce_Cd(Glaze_t005_angle)),Glaze_t005_angle,kind='quadratic')
Glaze_t01_angle =-data_t01_G[0,:]
Glaze_t01_Cl= -data_t01_G[11,:]
Glaze_t01_Cd= -data_t01_G[10,:]
fGlaze_t01_Cl   = interpolate.interp1d(Glaze_t01_Cl-func_noIce_Cl(Glaze_t01_angle),Glaze_t01_angle,kind='quadratic')
fGlaze_t01_Cd   = interpolate.interp1d((Glaze_t01_Cl/Glaze_t01_Cd-func_noIce_Cd(Glaze_t01_angle)),Glaze_t01_angle,kind='quadratic')
pdb.set_trace()
GlazePenalty_Cl=np.array([[0.005,fGlaze_t005_Cl(myval)],[0.0075,fGlaze_t0075_Cl(myval)],[0.01,fGlaze_t01_Cl(myval)]])
GlazePenalty_Cd=np.array([[0.01,fGlaze_t01_Cd(myvalCd)]])

ax1.plot(Horn_t005_angle,(Horn_t005_Cl - func_noIce_Cl(Horn_t005_angle))    ,alpha=lstyle['0.005'],color=cdict['horn'],lw=2)
ax1.plot(Horn_t01_angle,(Horn_t01_Cl - func_noIce_Cl(Horn_t01_angle))       ,alpha=lstyle['0.01'],color=cdict['horn'],lw=2)
ax1.plot(Horn_t015_angle,(Horn_t015_Cl - func_noIce_Cl(Horn_t015_angle))    ,alpha=lstyle['0.015'],color=cdict['horn'],lw=2)
ax1.plot(Horn_t025_angle,(Horn_t025_Cl - func_noIce_Cl(Horn_t025_angle))    ,alpha=lstyle['0.025'],color=cdict['horn'],lw=2, label="Horn")


ax1.plot(Rim_t0075_angle,(Rim_t0075_Cl - func_noIce_Cl(Rim_t0075_angle))    ,alpha=lstyle['0.0075'],color=cdict['rim'],lw=2)
ax1.plot(Rim_t015_angle,(Rim_t015_Cl - func_noIce_Cl(Rim_t015_angle))       ,alpha=lstyle['0.015'],color=cdict['rim'],lw=2)
ax1.plot(Rim_t025_angle,(Rim_t025_Cl - func_noIce_Cl(Rim_t025_angle))       ,alpha=lstyle['0.025'],color=cdict['rim'],lw=2)
ax1.plot(Rim_t05_angle,(Rim_t05_Cl - func_noIce_Cl(Rim_t05_angle))          ,alpha=lstyle['0.05'],color=cdict['rim'],lw=2, label="Rime")

ax1.plot(Glaze_t005_angle,(Glaze_t005_Cl - func_noIce_Cl(Glaze_t005_angle))    ,alpha=lstyle['0.005'],color=cdict['glaze'],lw=2)
ax1.plot(Glaze_t0075_angle,(Glaze_t0075_Cl - func_noIce_Cl(Glaze_t0075_angle))  ,alpha=lstyle['0.0075'],color=cdict['glaze'],lw=2)
ax1.plot(Glaze_t01_angle,(Glaze_t01_Cl - func_noIce_Cl(Glaze_t01_angle))        ,alpha=lstyle['0.01'],color=cdict['glaze'],lw=2, label="Glaze")
ax1.set_ylim(0,-0.7)
ax1.set_xlim(0,15)
ax1.legend()
ax1.set_xlabel("Angle of attack (degree)")
ax1.set_ylabel(r"$C_l$-penalty")



ax2.plot(Horn_t005_angle,(-Horn_t005_Cl/Horn_t005_Cd - func_noIce_ClCd(Horn_t005_angle))    ,alpha=lstyle['0.005'],color=cdict['horn'],lw=2)
ax2.plot(Horn_t01_angle,(-Horn_t01_Cl/Horn_t01_Cd - func_noIce_ClCd(Horn_t01_angle))        ,alpha=lstyle['0.01'],color=cdict['horn'],lw=2)
ax2.plot(Horn_t015_angle,(-Horn_t015_Cl/Horn_t015_Cd - func_noIce_ClCd(Horn_t015_angle))    ,alpha=lstyle['0.015'],color=cdict['horn'],lw=2)
ax2.plot(Horn_t025_angle,(-Horn_t025_Cl/Horn_t025_Cd - func_noIce_ClCd(Horn_t025_angle))    ,alpha=lstyle['0.025'],color=cdict['horn'],lw=2, label="Horn")


ax2.plot(Rim_t0075_angle,(-Rim_t0075_Cl/Rim_t0075_Cd - func_noIce_ClCd(Rim_t0075_angle))    ,alpha=lstyle['0.0075'],color=cdict['rim'],lw=2)
ax2.plot(Rim_t015_angle,(-Rim_t015_Cl/Rim_t015_Cd - func_noIce_ClCd(Rim_t015_angle))        ,alpha=lstyle['0.015'],color=cdict['rim'],lw=2)
ax2.plot(Rim_t025_angle,(-Rim_t025_Cl/Rim_t025_Cd - func_noIce_ClCd(Rim_t025_angle))        ,alpha=lstyle['0.025'],color=cdict['rim'],lw=2)
ax2.plot(Rim_t05_angle,(-Rim_t05_Cl/Rim_t05_Cd - func_noIce_ClCd(Rim_t05_angle))            ,alpha=lstyle['0.05'],color=cdict['rim'],lw=2, label="Rime")

ax2.plot(Glaze_t005_angle,(-Glaze_t005_Cl/Glaze_t005_Cd - func_noIce_ClCd(Glaze_t005_angle)),alpha=lstyle['0.005'],color=cdict['glaze'],lw=2)
ax2.plot(Glaze_t0075_angle,(-Glaze_t0075_Cl/Glaze_t0075_Cd - func_noIce_ClCd(Glaze_t0075_angle)),alpha=lstyle['0.0075'],color=cdict['glaze'],lw=2)
ax2.plot(Glaze_t01_angle,(-Glaze_t01_Cl/Glaze_t01_Cd - func_noIce_ClCd(Glaze_t01_angle)),alpha=lstyle['0.01'],color=cdict['glaze'],lw=2, label="Glaze")

#ax2.set_ylim(0,-2)
ax2.set_xlim(0,15)
ax1.legend()
ax2.set_ylabel(r"$C_l/C_d$-penalty")
ax2.set_xlabel("Angle of attack (degree)")
plt.tight_layout()
plt.savefig("Penalty_allCases"+myformat)
plt.show()



fig, [ax1,ax2] = plt.subplots(1,2,figsize=set_twowide(width))
ax1.plot(HornPenalty_Cl[:,0],HornPenalty_Cl[:,1],color=cdict['horn'],lw=2, label="Horn")
ax1.plot(GlazePenalty_Cl[:,0],GlazePenalty_Cl[:,1],color=cdict['glaze'],lw=2, label="Glaze")
ax1.plot(RimPenalty_Cl[:,0],RimPenalty_Cl[:,1],color=cdict['rim'],lw=2, label="Rime")

#ax2.plot(HornPenalty_Cd[:,0],HornPenalty_Cd[:,1],color=cdict['horn'],lw=2, label="Horn")
#ax2.plot(GlazePenalty_Cd[:,0],GlazePenalty_Cd[:,1],color=cdict['glaze'],lw=2, label="Glaze")
#ax2.plot(RimPenalty_Cd[:,0],RimPenalty_Cd[:,1],color=cdict['rim'],lw=2, label="Rim")
ax2.plot(Horn_thick,maxAngle-Horn_angle,color=cdict['horn'],lw=2, label="Horn")
ax2.plot(Glaze_thick,maxAngle-Glaze_angle,color=cdict['glaze'],lw=2, label="Glaze")
ax2.plot(Rime_thick,maxAngle-Rime_angle,color=cdict['rim'],lw=2, label="Rime")


ax1.legend()
ax1.set_ylabel("angle of attack (degree)")
ax1.set_xlabel("ice thickness/chord")
ax2.set_ylabel("stall penalty (degree)")
ax2.set_xlabel("ice thickness/chord")

plt.tight_layout()
plt.savefig("Separation_AoA"+myformat)
plt.show()

