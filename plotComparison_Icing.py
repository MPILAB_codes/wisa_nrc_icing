def set_size(width, fraction=1):
    """ Set aesthetic figure dimensions to avoid scaling in latex.

    Parameters
    ----------
    width: float
            Width in pts
    fraction: float
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim
def set_twowide(width, fraction=1):
    """ Set aesthetic figure dimensions to avoid scaling in latex.

    Parameters
    ----------
    width: float
            Width in pts
    fraction: float
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio/1.08

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim



import os
print(os.environ['PATH'])
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt


width = 468
myformat='.pdf'

nice_fonts = {
      # Use LaTex to write all text
        "text.usetex": True,
        "font.family": "serif",
      # Use 10pt font in plots, to match 10pt font in document
        "axes.labelsize": 11,
        "font.size": 11,
      # Make the legend/label fonts a little smaller
        "legend.fontsize": 10,
        "xtick.labelsize": 10,
        "ytick.labelsize": 10,
}

mpl.rcParams.update(nice_fonts)

#data_t05=np.loadtxt("thick_025/SU2_HORN_t025.dat", unpack=True)  
data_HORN_t025=np.loadtxt("SU2_HORN_thickness/thick_025/SU2_HORN_t025.dat", unpack=True)   
data_HORN_t015=np.loadtxt("SU2_HORN_thickness/thick_015/SU2_HORN_t015.dat", unpack=True)    # RUN ON CERTAINTY!!! 

data_GLAZE_t01=np.loadtxt("SU2_thickness_d025/thick_01/SU2_t01_d025.dat", unpack=True)
data_GLAZE_t005=np.loadtxt("SU2_thickness_d025/thick_005/SU2_t005_d025.dat", unpack=True)
data_GLAZE_t0075=np.loadtxt("SU2_thickness_d025/thick_0075/SU2_t0075_d025.dat", unpack=True)

data_RIM_t05=np.loadtxt("SU2_RIM_thickness/thick_05/SU2_RIM_thick05.dat", unpack=True)
data_RIM_t025=np.loadtxt("SU2_RIM_thickness/thick_025/SU2_RIM_thick025.dat", unpack=True)   # RUN ON CERTAINTY!!!
data_RIM_t015=np.loadtxt("SU2_RIM_thickness/thick_015/SU2_RIM_thick015.dat", unpack=True)    # RUN ON CERTAINTY!!!
data_RIM_t0075=np.loadtxt("SU2_RIM_thickness/thick_0075/SU2_RIM_thick0075.dat", unpack=True)

dataSU2_valid=np.loadtxt("./SU2_validation/validationData/SU2_superFine.dat", unpack=True)

import pdb
fig, [ax1,ax2] = plt.subplots(1,2,figsize=set_twowide(width))
ax1.scatter(dataSU2_valid[0,:],dataSU2_valid[11,:],marker="P",color='b', label="no ice")
ax1.scatter(-dataSU2_valid[0,:],-dataSU2_valid[11,:],marker="P",color='b')


ax1.plot(-data_HORN_t015[0,:],-data_HORN_t015[10,:],'r', label="HORN 0.015")
ax1.plot(-data_GLAZE_t01[0,:],-data_GLAZE_t01[11,:],'g', label="GLAZE 0.015")
ax1.plot(-data_RIM_t015[0,:],-data_RIM_t015[10,:],'k', label="RIM 0.0075")
ax1.set_ylim(0,1.7)
ax1.set_xlim(0,15)
ax1.legend()
ax1.set_xlabel("Angle of attack")
ax1.set_ylabel(r"$C_l$")



pdb.set_trace()
ax2.scatter(-dataSU2_valid[11,:],-dataSU2_valid[10,:],marker="P",color='b', label="no ice")
ax2.scatter(-dataSU2_valid[11,:],dataSU2_valid[10,:],marker="P",color='b' )
#ax2.plot(-data_t05[11,:],data_t05[10,:],'b', label="thickness 0.01")
ax2.plot(-data_HORN_t015[10,:],data_HORN_t015[9,:],'r', label="thickness 0.025")
ax2.plot(-data_GLAZE_t01[11,:],data_GLAZE_t01[10,:],'g', label="thickness 0.015")
ax2.plot(-data_RIM_t015[10,:],data_RIM_t015[9,:],'k', label="thickness 0.0075")
ax2.set_ylim(0,0.03)
ax2.set_xlim(0,1.5)
ax1.legend()
ax2.set_ylabel(r"$C_d$")
ax2.set_xlabel(r"$C_l$")
plt.tight_layout()
plt.savefig("Glaze_depth_Cl_Cd"+myformat)
plt.show()


plt.figure(figsize=set_size(width))
plt.scatter(-dataSU2_valid[0,:],-dataSU2_valid[11,:]/dataSU2_valid[10,:],marker="P",color='b', label="no ice")
#plt.plot(-data_t05[0,:],-data_t05[11,:]/data_t05[10,:],'b', label="depth 0.05")
plt.plot(-data_HORN_t015[0,:],-data_HORN_t015[10,:]/data_HORN_t015[9,:],'r', label="depth 0.025")
plt.plot(-data_GLAZE_t01[0,:],-data_GLAZE_t01[11,:]/data_GLAZE_t01[10,:],'g', label="depth 0.015")
plt.plot(-data_RIM_t015[0,:],-data_RIM_t015[10,:]/data_RIM_t015[9,:],'k', label="depth 0.0075")
plt.legend()
plt.ylabel(r"$C_l/C_d$")
plt.xlabel("Angle of attack")
plt.tight_layout()
plt.savefig("Glaze_depth_Cl_over_Cd"+myformat)
plt.show()