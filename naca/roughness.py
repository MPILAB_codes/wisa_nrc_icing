import numpy as np
import matplotlib.pyplot as plt


import random

random.seed(10)
print(random.random())


s = np.random.poisson(1.5, 10000)

x=np.linspace(0,1,59)

t = np.arange(200)
n = np.zeros((200,), dtype=complex)
n[5:15] = np.exp(1j*np.random.uniform(0, 2*np.pi, (10,)))
s = np.fft.ifft(n)
#plt.plot(t, s.real, label='real')
#plt.show()


#https://en.wikipedia.org/wiki/Butterfly_curve_(transcendental)
t=np.linspace(0,6.28,1000)
coeff=0.1
y=coeff*np.sin(t)*(np.exp(np.cos(t))-2*np.cos(4*t)-(np.sin(t/12.0))**5)/4.059850964394309
x=-coeff*np.cos(t)*(np.exp(np.cos(t))-2*np.cos(4*t)-(np.sin(t/12.0))**5)/4.059850964394309
dist=np.sqrt(x**2+y**2)

theta=np.linspace(0,6.28/4,1000)
r= np.exp(np.sin(theta))-2*np.cos(4*theta)+(np.sin(1/24*(2*theta-np.pi)))**5
fig, ax = plt.subplots(subplot_kw={'projection': 'polar'})
ax.plot(theta, r)
plt.show()
print(np.max(dist))


plt.plot(x,y)
plt.show()