def set_size(width, fraction=1):
    """ Set aesthetic figure dimensions to avoid scaling in latex.

    Parameters
    ----------
    width: float
            Width in pts
    fraction: float
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim
def set_narrow(width, fraction=1):
    """ Set aesthetic figure dimensions to avoid scaling in latex.

    Parameters
    ----------
    width: float
            Width in pts
    fraction: float
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = (fig_width_pt * inches_per_pt) * golden_ratio*1.5

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim
def set_twowide(width, fraction=1):
    """ Set aesthetic figure dimensions to avoid scaling in latex.

    Parameters
    ----------
    width: float
            Width in pts
    fraction: float
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio/1.08

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim



import os
print(os.environ['PATH'])
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt


width = 468
myformat='.pdf'

nice_fonts = {
      # Use LaTex to write all text
        "text.usetex": True,
        "font.family": "serif",
      # Use 10pt font in plots, to match 10pt font in document
        "axes.labelsize": 11,
        "font.size": 11,
      # Make the legend/label fonts a little smaller
        "legend.fontsize": 9,
        "xtick.labelsize": 10,
        "ytick.labelsize": 10,
}

cdict = {'0.005': '#FED976',
            '0.75\%c':'#FEB24C',
            '0.01':'#FD8D3C',
            '1.5\%c':'#FC4E2A', 
            '2.5\%c':'#E31A1C',
            '5.0\%c': '#B10026'}
            
mpl.rcParams.update(nice_fonts)

data_t05=np.loadtxt("thick_05/SU2_RIM_thick05.dat", unpack=True)  
data_t025=np.loadtxt("thick_025/SU2_RIM_thick025.dat", unpack=True)   # RUN ON CERTAINTY!!! 
data_t015=np.loadtxt("thick_015/SU2_RIM_thick015.dat", unpack=True)    # RUN ON CERTAINTY!!! 
data_t0075=np.loadtxt("thick_0075/SU2_RIM_thick0075.dat", unpack=True)

dataSU2_valid=np.loadtxt("../SU2_validation/validationData/SU2_superFine.dat", unpack=True)

import pdb
#%CMz =15
#%CFx =16
myindex=15
fig, ax1 = plt.subplots(1,1,figsize=set_narrow(width/2.0))
ax1.scatter(dataSU2_valid[0,:]  ,dataSU2_valid[myindex,:]   ,marker="o",color='b', label="no ice",facecolor='none')
ax1.scatter(-dataSU2_valid[0,:] ,-dataSU2_valid[myindex,:]  ,marker="o",color='b',facecolor='none')

ax1.plot(-data_t0075[0,:]       ,-data_t0075[myindex,:]     ,color=cdict['0.75\%c'], label="thickness 0.75\%c")
ax1.plot(-data_t015[0,:]        ,-data_t015[myindex-1,:]    ,color=cdict['1.5\%c'], label="thickness 1.5\%c")
ax1.plot(-data_t025[0,:]        ,-data_t025[myindex-1,:]      ,color=cdict['2.5\%c'], label="thickness 2.5\%c")
ax1.plot(-data_t05[0,:]        ,-data_t05[myindex,:]        ,color=cdict['5.0\%c'], label="thickness 5.0\%c")

myindex=17
#ax2.scatter(dataSU2_valid[0,:]  ,dataSU2_valid[myindex,:]   ,marker="o",color='b', label="no ice",facecolor='none')
#ax2.scatter(-dataSU2_valid[0,:] ,-dataSU2_valid[myindex,:]  ,marker="o",color='b',facecolor='none')
#ax2.plot(-data_t005_d025[0,:]   ,-data_t005_d025[myindex,:] ,color=cdict['0.005'], label="thickness 0.005")
#ax2.plot(-data_t0075_d025[0,:]  ,-data_t0075_d025[myindex,:],color=cdict['0.75\%c'], label="thickness 0.75\%c")
#ax2.plot(-data_t01_d025[0,:]    ,-data_t01_d025[myindex,:]  ,color=cdict['0.01'], label="thickness 0.01")
ax1.set_xlim(0,15)
#ax2.set_xlim(0,15)
#ax2.set_ylim(0,1.5)
ax1.set_xlabel("Angle of attack (degree)")
ax1.set_ylabel(r"$M_z$")
#ax2.set_xlabel("Angle of attack")
#ax2.set_ylabel(r"$Cf_y$")
ax1.legend()
plt.tight_layout()
plt.savefig("Glaze_thickness_moments"+myformat)
plt.show()




fig, [ax1,ax2] = plt.subplots(1,2,figsize=set_twowide(width))

ax1.scatter(dataSU2_valid[0,:],dataSU2_valid[11,:],marker="o",color='b', label="no ice",facecolor='none')
ax1.scatter(-dataSU2_valid[0,:],-dataSU2_valid[11,:],marker="o",color='b',facecolor='none')

ax1.plot(-data_t0075[0,:],-data_t0075[11,:] ,color=cdict['0.75\%c'], label="thickness 0.75\%c")
ax1.plot(-data_t015[0,:],-data_t015[10,:]   ,color=cdict['1.5\%c'], label="thickness 1.5\%c")
ax1.plot(-data_t025[0,:],-data_t025[10,:]   ,color=cdict['2.5\%c'], label="thickness 2.5\%c")
ax1.plot(-data_t05[0,:],-data_t05[11,:]     ,color=cdict['5.0\%c'], label="thickness 5.0\%c")



ax1.set_ylim(0,1.7)
ax1.set_xlim(0,15)
ax1.legend()
ax1.set_xlabel("Angle of attack (degree)")
ax1.set_ylabel(r"$C_l$")


ax2.scatter(-dataSU2_valid[11,:],-dataSU2_valid[10,:],marker="o",color='b', label="no ice",facecolor='none')
ax2.scatter(-dataSU2_valid[11,:],dataSU2_valid[10,:],marker="o",color='b',facecolor='none' )
ax2.plot(-data_t0075[11,:],data_t0075[10,:] ,color=cdict['0.75\%c'], label="thickness 0.75\%c")
ax2.plot(-data_t015[10,:],data_t015[9,:]    ,color=cdict['1.5\%c'], label="thickness 1.5\%c")
ax2.plot(-data_t025[10,:],data_t025[9,:]    ,color=cdict['2.5\%c'], label="thickness 2.5\%c")
ax2.plot(-data_t05[11,:],data_t05[10,:]     ,color=cdict['5.0\%c'], label="thickness 5.0\%c")



ax2.set_ylim(0,0.03)
ax2.set_xlim(0,1.5)
ax1.legend()
ax2.set_ylabel(r"$C_d$")
ax2.set_xlabel(r"$C_l$")
plt.tight_layout()

plt.savefig("RIM_thickness_Cl_Cd"+myformat)
plt.show()


plt.figure(figsize=set_narrow(width/2))
plt.scatter(-dataSU2_valid[0,:],-dataSU2_valid[11,:]/dataSU2_valid[10,:],marker="o",color='b', label="no ice",facecolor='none')
plt.plot(-data_t0075[0,:],-data_t0075[11,:]/data_t0075[10,:],color=cdict['0.75\%c'], label="thickness 0.75\%c")
plt.plot(-data_t015[0,:],-data_t015[10,:]/data_t015[9,:]    ,color=cdict['1.5\%c'], label="thickness 1.5\%c")
plt.plot(-data_t025[0,:],-data_t025[10,:]/data_t025[9,:]    ,color=cdict['2.5\%c'], label="thickness 2.5\%c")
plt.plot(-data_t05[0,:],-data_t05[11,:]/data_t05[10,:]      ,color=cdict['5.0\%c'], label="thickness 5.0\%c")



plt.legend(loc=8,labelspacing=0.1)
plt.ylabel(r"$C_l/C_d$")
plt.xlabel("Angle of attack (degree)")
plt.text(1,75,"Rime", fontsize=12)
plt.tight_layout()
plt.savefig("RIM_thickness_Cl_over_Cd"+myformat)
plt.show()
